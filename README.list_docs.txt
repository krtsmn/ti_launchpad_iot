- driver_library_peripheral_spmu298a.pdf - TivaWareTM Peripheral Driver Library USER’S GUIDE
	a) Direct Register programming macros description;
	b) ROM API description;
	doc size - 707 pages.
- laa542_build_your_own_launchpad.pdf - Build Your Own LaunchPadTM or LaunchPad BoosterPackTM Development Tool
	Description of 'Launchpad - boosterpacks' interface, pins, standards etc.
	doc size - 8 pages.
- spmu365a_tm4c1294kit_ug.pdf - TivaTM C Series TM4C1294 Connected LaunchPad Evaluation Kit EK-TM4C1294XL
				User's Guide, rev. march 2014
	common knowledge about hardware, software development, flash programming.
- spmu365b_tm4c1294kit_ug.pdf - TivaTM C Series TM4C1294 Connected LaunchPad Evaluation Kit EK-TM4C1294XL
				User's Guide, rev. may 2015
	look at rev. march 2015, may be some errors are corrected or/and details are added.
- spmz858_meet_tm4c1294.pdf - common short knowledge. Very small document with pictures.

- swru331a_cc3000_UG.pdf - TI CC3000 BoosterPackBoard User's Guide
	jumpers description;
	antenna description;
	flash with firmware description;
	pictures.
- tm4c1294ncpdt.pdf - TivaTM TM4C1294NCPDT Microcontroller. Data sheet.
	microcontroller contains a lot of ROM, flash, EEPROM, SRAM memory and periphery.
	on-chip flash: 0..0x000fffff
	reserved: 0x00100000..0x01ffffff
	on-chip ROM: 0x02000000..0x02ffffff
	reserved: 0x03000000..0x1fffffff
	SRAM start address 0x20000000.
	etc: look at page 102.
	processor features description:
		programming model;
		memory model;
		exception model;
		fault handling;
		power management;
		instruction set summary.
	on-chip periphery:
		system timer;
		interrupt controller;
		system control block;
		MPU - memory protection unit;
		FPU - floating point unit.
	jtag interface;
	GPIO;
	EPI - External Periperal Interface;
	CRC - CRC support;
	WDT - watch dog timers;
	ADC - analog to digital converter;
	UARTS;
	QSSI - quad syncronous serial interface;
	I2C;
	CAN;
	Ethernet controller;
	USB;
	Analog comparators;
	PWM - pulse width modulator;
	QEI - quadrature encoder interface;
- SW_TM4C-2.1.0.12573/docs:
	- SW-EK-TM4C1294XL-BOOST-CC3000-UG-2.1.0.12573.pdf - boosterpack, WiFi.
	- SW-EK-TM4C1294XL-BOOST-DLPTRF7970ABP-UG-2.1.0.12573.pdf - firmware development package.
		board-specific drivers and example applications that are provided
		for this boosterpack.
	- EK-TM4C1294XL-BOOSTXL-BATTPACK - Firmware Development Package. Board-specific drivers and
		 example applications that are provided for this boosterpack


