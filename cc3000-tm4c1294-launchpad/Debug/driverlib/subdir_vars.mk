################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../driverlib/cpu.c \
../driverlib/fpu.c \
../driverlib/gpio.c \
../driverlib/interrupt.c \
../driverlib/ssi.c \
../driverlib/sysctl.c \
../driverlib/systick.c \
../driverlib/timer.c \
../driverlib/uart.c \
../driverlib/udma.c 

OBJS += \
./driverlib/cpu.obj \
./driverlib/fpu.obj \
./driverlib/gpio.obj \
./driverlib/interrupt.obj \
./driverlib/ssi.obj \
./driverlib/sysctl.obj \
./driverlib/systick.obj \
./driverlib/timer.obj \
./driverlib/uart.obj \
./driverlib/udma.obj 

C_DEPS += \
./driverlib/cpu.pp \
./driverlib/fpu.pp \
./driverlib/gpio.pp \
./driverlib/interrupt.pp \
./driverlib/ssi.pp \
./driverlib/sysctl.pp \
./driverlib/systick.pp \
./driverlib/timer.pp \
./driverlib/uart.pp \
./driverlib/udma.pp 

C_DEPS__QUOTED += \
"driverlib\cpu.pp" \
"driverlib\fpu.pp" \
"driverlib\gpio.pp" \
"driverlib\interrupt.pp" \
"driverlib\ssi.pp" \
"driverlib\sysctl.pp" \
"driverlib\systick.pp" \
"driverlib\timer.pp" \
"driverlib\uart.pp" \
"driverlib\udma.pp" 

OBJS__QUOTED += \
"driverlib\cpu.obj" \
"driverlib\fpu.obj" \
"driverlib\gpio.obj" \
"driverlib\interrupt.obj" \
"driverlib\ssi.obj" \
"driverlib\sysctl.obj" \
"driverlib\systick.obj" \
"driverlib\timer.obj" \
"driverlib\uart.obj" \
"driverlib\udma.obj" 

C_SRCS__QUOTED += \
"../driverlib/cpu.c" \
"../driverlib/fpu.c" \
"../driverlib/gpio.c" \
"../driverlib/interrupt.c" \
"../driverlib/ssi.c" \
"../driverlib/sysctl.c" \
"../driverlib/systick.c" \
"../driverlib/timer.c" \
"../driverlib/uart.c" \
"../driverlib/udma.c" 


