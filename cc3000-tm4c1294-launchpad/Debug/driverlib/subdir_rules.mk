################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Each subdirectory must supply rules for building sources it contributes
driverlib/cpu.obj: ../driverlib/cpu.c $(GEN_OPTS) $(GEN_SRCS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv5/tools/compiler/arm_5.0.4/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 --abi=eabi -me -g --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad" --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad/cc3000/drivers/ek-tm4c129" --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad/cc3000/src/uart" --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad/cc3000/src/spi" --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad/cc3000/src/include" --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad/cc3000/src/core_driver" --include_path="C:/ti/ccsv5/tools/compiler/arm_5.0.4/include" --define=ccs --define=UART_BUFFERED --define=TARGET_IS_SNOWFLAKE_RA1 --define=PART_TM4C1294NCPDT --define=CC3000_USE_BOOSTERPACK2 --diag_warning=225 --display_error_number --diag_wrap=off --preproc_with_compile --preproc_dependency="driverlib/cpu.pp" --obj_directory="driverlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

driverlib/fpu.obj: ../driverlib/fpu.c $(GEN_OPTS) $(GEN_SRCS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv5/tools/compiler/arm_5.0.4/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 --abi=eabi -me -g --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad" --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad/cc3000/drivers/ek-tm4c129" --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad/cc3000/src/uart" --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad/cc3000/src/spi" --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad/cc3000/src/include" --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad/cc3000/src/core_driver" --include_path="C:/ti/ccsv5/tools/compiler/arm_5.0.4/include" --define=ccs --define=UART_BUFFERED --define=TARGET_IS_SNOWFLAKE_RA1 --define=PART_TM4C1294NCPDT --define=CC3000_USE_BOOSTERPACK2 --diag_warning=225 --display_error_number --diag_wrap=off --preproc_with_compile --preproc_dependency="driverlib/fpu.pp" --obj_directory="driverlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

driverlib/gpio.obj: ../driverlib/gpio.c $(GEN_OPTS) $(GEN_SRCS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv5/tools/compiler/arm_5.0.4/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 --abi=eabi -me -g --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad" --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad/cc3000/drivers/ek-tm4c129" --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad/cc3000/src/uart" --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad/cc3000/src/spi" --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad/cc3000/src/include" --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad/cc3000/src/core_driver" --include_path="C:/ti/ccsv5/tools/compiler/arm_5.0.4/include" --define=ccs --define=UART_BUFFERED --define=TARGET_IS_SNOWFLAKE_RA1 --define=PART_TM4C1294NCPDT --define=CC3000_USE_BOOSTERPACK2 --diag_warning=225 --display_error_number --diag_wrap=off --preproc_with_compile --preproc_dependency="driverlib/gpio.pp" --obj_directory="driverlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

driverlib/interrupt.obj: ../driverlib/interrupt.c $(GEN_OPTS) $(GEN_SRCS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv5/tools/compiler/arm_5.0.4/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 --abi=eabi -me -g --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad" --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad/cc3000/drivers/ek-tm4c129" --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad/cc3000/src/uart" --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad/cc3000/src/spi" --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad/cc3000/src/include" --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad/cc3000/src/core_driver" --include_path="C:/ti/ccsv5/tools/compiler/arm_5.0.4/include" --define=ccs --define=UART_BUFFERED --define=TARGET_IS_SNOWFLAKE_RA1 --define=PART_TM4C1294NCPDT --define=CC3000_USE_BOOSTERPACK2 --diag_warning=225 --display_error_number --diag_wrap=off --preproc_with_compile --preproc_dependency="driverlib/interrupt.pp" --obj_directory="driverlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

driverlib/ssi.obj: ../driverlib/ssi.c $(GEN_OPTS) $(GEN_SRCS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv5/tools/compiler/arm_5.0.4/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 --abi=eabi -me -g --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad" --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad/cc3000/drivers/ek-tm4c129" --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad/cc3000/src/uart" --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad/cc3000/src/spi" --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad/cc3000/src/include" --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad/cc3000/src/core_driver" --include_path="C:/ti/ccsv5/tools/compiler/arm_5.0.4/include" --define=ccs --define=UART_BUFFERED --define=TARGET_IS_SNOWFLAKE_RA1 --define=PART_TM4C1294NCPDT --define=CC3000_USE_BOOSTERPACK2 --diag_warning=225 --display_error_number --diag_wrap=off --preproc_with_compile --preproc_dependency="driverlib/ssi.pp" --obj_directory="driverlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

driverlib/sysctl.obj: ../driverlib/sysctl.c $(GEN_OPTS) $(GEN_SRCS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv5/tools/compiler/arm_5.0.4/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 --abi=eabi -me -g --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad" --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad/cc3000/drivers/ek-tm4c129" --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad/cc3000/src/uart" --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad/cc3000/src/spi" --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad/cc3000/src/include" --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad/cc3000/src/core_driver" --include_path="C:/ti/ccsv5/tools/compiler/arm_5.0.4/include" --define=ccs --define=UART_BUFFERED --define=TARGET_IS_SNOWFLAKE_RA1 --define=PART_TM4C1294NCPDT --define=CC3000_USE_BOOSTERPACK2 --diag_warning=225 --display_error_number --diag_wrap=off --preproc_with_compile --preproc_dependency="driverlib/sysctl.pp" --obj_directory="driverlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

driverlib/systick.obj: ../driverlib/systick.c $(GEN_OPTS) $(GEN_SRCS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv5/tools/compiler/arm_5.0.4/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 --abi=eabi -me -g --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad" --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad/cc3000/drivers/ek-tm4c129" --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad/cc3000/src/uart" --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad/cc3000/src/spi" --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad/cc3000/src/include" --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad/cc3000/src/core_driver" --include_path="C:/ti/ccsv5/tools/compiler/arm_5.0.4/include" --define=ccs --define=UART_BUFFERED --define=TARGET_IS_SNOWFLAKE_RA1 --define=PART_TM4C1294NCPDT --define=CC3000_USE_BOOSTERPACK2 --diag_warning=225 --display_error_number --diag_wrap=off --preproc_with_compile --preproc_dependency="driverlib/systick.pp" --obj_directory="driverlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

driverlib/timer.obj: ../driverlib/timer.c $(GEN_OPTS) $(GEN_SRCS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv5/tools/compiler/arm_5.0.4/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 --abi=eabi -me -g --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad" --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad/cc3000/drivers/ek-tm4c129" --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad/cc3000/src/uart" --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad/cc3000/src/spi" --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad/cc3000/src/include" --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad/cc3000/src/core_driver" --include_path="C:/ti/ccsv5/tools/compiler/arm_5.0.4/include" --define=ccs --define=UART_BUFFERED --define=TARGET_IS_SNOWFLAKE_RA1 --define=PART_TM4C1294NCPDT --define=CC3000_USE_BOOSTERPACK2 --diag_warning=225 --display_error_number --diag_wrap=off --preproc_with_compile --preproc_dependency="driverlib/timer.pp" --obj_directory="driverlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

driverlib/uart.obj: ../driverlib/uart.c $(GEN_OPTS) $(GEN_SRCS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv5/tools/compiler/arm_5.0.4/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 --abi=eabi -me -g --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad" --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad/cc3000/drivers/ek-tm4c129" --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad/cc3000/src/uart" --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad/cc3000/src/spi" --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad/cc3000/src/include" --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad/cc3000/src/core_driver" --include_path="C:/ti/ccsv5/tools/compiler/arm_5.0.4/include" --define=ccs --define=UART_BUFFERED --define=TARGET_IS_SNOWFLAKE_RA1 --define=PART_TM4C1294NCPDT --define=CC3000_USE_BOOSTERPACK2 --diag_warning=225 --display_error_number --diag_wrap=off --preproc_with_compile --preproc_dependency="driverlib/uart.pp" --obj_directory="driverlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

driverlib/udma.obj: ../driverlib/udma.c $(GEN_OPTS) $(GEN_SRCS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv5/tools/compiler/arm_5.0.4/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 --abi=eabi -me -g --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad" --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad/cc3000/drivers/ek-tm4c129" --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad/cc3000/src/uart" --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad/cc3000/src/spi" --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad/cc3000/src/include" --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad/cc3000/src/core_driver" --include_path="C:/ti/ccsv5/tools/compiler/arm_5.0.4/include" --define=ccs --define=UART_BUFFERED --define=TARGET_IS_SNOWFLAKE_RA1 --define=PART_TM4C1294NCPDT --define=CC3000_USE_BOOSTERPACK2 --diag_warning=225 --display_error_number --diag_wrap=off --preproc_with_compile --preproc_dependency="driverlib/udma.pp" --obj_directory="driverlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '


