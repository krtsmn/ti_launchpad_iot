################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../cc3000/src/core_driver/cc3000_common.c \
../cc3000/src/core_driver/evnt_handler.c \
../cc3000/src/core_driver/hci.c \
../cc3000/src/core_driver/netapp.c \
../cc3000/src/core_driver/nvmem.c \
../cc3000/src/core_driver/security.c \
../cc3000/src/core_driver/socket.c \
../cc3000/src/core_driver/wlan.c 

OBJS += \
./cc3000/src/core_driver/cc3000_common.obj \
./cc3000/src/core_driver/evnt_handler.obj \
./cc3000/src/core_driver/hci.obj \
./cc3000/src/core_driver/netapp.obj \
./cc3000/src/core_driver/nvmem.obj \
./cc3000/src/core_driver/security.obj \
./cc3000/src/core_driver/socket.obj \
./cc3000/src/core_driver/wlan.obj 

C_DEPS += \
./cc3000/src/core_driver/cc3000_common.pp \
./cc3000/src/core_driver/evnt_handler.pp \
./cc3000/src/core_driver/hci.pp \
./cc3000/src/core_driver/netapp.pp \
./cc3000/src/core_driver/nvmem.pp \
./cc3000/src/core_driver/security.pp \
./cc3000/src/core_driver/socket.pp \
./cc3000/src/core_driver/wlan.pp 

C_DEPS__QUOTED += \
"cc3000\src\core_driver\cc3000_common.pp" \
"cc3000\src\core_driver\evnt_handler.pp" \
"cc3000\src\core_driver\hci.pp" \
"cc3000\src\core_driver\netapp.pp" \
"cc3000\src\core_driver\nvmem.pp" \
"cc3000\src\core_driver\security.pp" \
"cc3000\src\core_driver\socket.pp" \
"cc3000\src\core_driver\wlan.pp" 

OBJS__QUOTED += \
"cc3000\src\core_driver\cc3000_common.obj" \
"cc3000\src\core_driver\evnt_handler.obj" \
"cc3000\src\core_driver\hci.obj" \
"cc3000\src\core_driver\netapp.obj" \
"cc3000\src\core_driver\nvmem.obj" \
"cc3000\src\core_driver\security.obj" \
"cc3000\src\core_driver\socket.obj" \
"cc3000\src\core_driver\wlan.obj" 

C_SRCS__QUOTED += \
"../cc3000/src/core_driver/cc3000_common.c" \
"../cc3000/src/core_driver/evnt_handler.c" \
"../cc3000/src/core_driver/hci.c" \
"../cc3000/src/core_driver/netapp.c" \
"../cc3000/src/core_driver/nvmem.c" \
"../cc3000/src/core_driver/security.c" \
"../cc3000/src/core_driver/socket.c" \
"../cc3000/src/core_driver/wlan.c" 


