################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Each subdirectory must supply rules for building sources it contributes
cc3000/src/spi/spi-tm4c129.obj: ../cc3000/src/spi/spi-tm4c129.c $(GEN_OPTS) $(GEN_SRCS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv5/tools/compiler/arm_5.0.4/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 --abi=eabi -me -g --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad" --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad/cc3000/drivers/ek-tm4c129" --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad/cc3000/src/uart" --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad/cc3000/src/spi" --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad/cc3000/src/include" --include_path="C:/Users/gkurtsman/ti/workspace/cc3000-tm4c1294-launchpad/cc3000/src/core_driver" --include_path="C:/ti/ccsv5/tools/compiler/arm_5.0.4/include" --define=ccs --define=UART_BUFFERED --define=TARGET_IS_SNOWFLAKE_RA1 --define=PART_TM4C1294NCPDT --define=CC3000_USE_BOOSTERPACK2 --diag_warning=225 --display_error_number --diag_wrap=off --preproc_with_compile --preproc_dependency="cc3000/src/spi/spi-tm4c129.pp" --obj_directory="cc3000/src/spi" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '


