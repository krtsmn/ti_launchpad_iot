################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Each subdirectory must supply rules for building sources it contributes
driverlib/cpu.obj: ../driverlib/cpu.c $(GEN_OPTS) $(GEN_SRCS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv5/tools/compiler/arm_5.0.4/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 --abi=eabi -me -g --include_path="C:/Users/gkurtsman/ti/workspace/tm4c1294-launchpad" --include_path="C:/ti/ccsv5/tools/compiler/arm_5.0.4/include" --include_path="C:/Users/gkurtsman/ti/workspace/tm4c1294-launchpad/cc3000/drivers/ek-tm4c129" --define=PART_TM4C1294NCPDT --define=TARGET_IS_SNOWFLAKE_RA1 --define=ccs --diag_warning=225 --display_error_number --diag_wrap=off --preproc_with_compile --preproc_dependency="driverlib/cpu.pp" --obj_directory="driverlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

driverlib/gpio.obj: ../driverlib/gpio.c $(GEN_OPTS) $(GEN_SRCS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv5/tools/compiler/arm_5.0.4/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 --abi=eabi -me -g --include_path="C:/Users/gkurtsman/ti/workspace/tm4c1294-launchpad" --include_path="C:/ti/ccsv5/tools/compiler/arm_5.0.4/include" --include_path="C:/Users/gkurtsman/ti/workspace/tm4c1294-launchpad/cc3000/drivers/ek-tm4c129" --define=PART_TM4C1294NCPDT --define=TARGET_IS_SNOWFLAKE_RA1 --define=ccs --diag_warning=225 --display_error_number --diag_wrap=off --preproc_with_compile --preproc_dependency="driverlib/gpio.pp" --obj_directory="driverlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

driverlib/interrupt.obj: ../driverlib/interrupt.c $(GEN_OPTS) $(GEN_SRCS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv5/tools/compiler/arm_5.0.4/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 --abi=eabi -me -g --include_path="C:/Users/gkurtsman/ti/workspace/tm4c1294-launchpad" --include_path="C:/ti/ccsv5/tools/compiler/arm_5.0.4/include" --include_path="C:/Users/gkurtsman/ti/workspace/tm4c1294-launchpad/cc3000/drivers/ek-tm4c129" --define=PART_TM4C1294NCPDT --define=TARGET_IS_SNOWFLAKE_RA1 --define=ccs --diag_warning=225 --display_error_number --diag_wrap=off --preproc_with_compile --preproc_dependency="driverlib/interrupt.pp" --obj_directory="driverlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

driverlib/sysctl.obj: ../driverlib/sysctl.c $(GEN_OPTS) $(GEN_SRCS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv5/tools/compiler/arm_5.0.4/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 --abi=eabi -me -g --include_path="C:/Users/gkurtsman/ti/workspace/tm4c1294-launchpad" --include_path="C:/ti/ccsv5/tools/compiler/arm_5.0.4/include" --include_path="C:/Users/gkurtsman/ti/workspace/tm4c1294-launchpad/cc3000/drivers/ek-tm4c129" --define=PART_TM4C1294NCPDT --define=TARGET_IS_SNOWFLAKE_RA1 --define=ccs --diag_warning=225 --display_error_number --diag_wrap=off --preproc_with_compile --preproc_dependency="driverlib/sysctl.pp" --obj_directory="driverlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '


